package com.goostree.simpletipcalculator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import android.app.Application;
import android.content.Context;
import android.widget.Toast;
//
// Model for SimpleTipTracker - Extends Application so its data exists for entire scope of the app
//
public class TipRecord extends Application implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FILENAME = "simpleTipTracker.ser";
	public static final String BACKUP_FILENAME = "simpleTipTrackerBackup.ser";
	private ArrayList<TipEntry> tipLog;
	private ArrayList<String> tipeeList;
	private double payRate;
	private int weekStart;
	private boolean disableCalendar;

	public TipRecord(){
		tipLog = new ArrayList<TipEntry>();
		tipeeList = new ArrayList<String>();
		payRate = 0;
		weekStart = 1;
		disableCalendar = false;
		
		/*
		comparator = new Comparator<TipEntry>() {
 
            @Override
            public int compare(TipEntry o1, TipEntry o2) {
                return o1.compareTo(o2);
            }
        }; //set comparator */
		
	}
	
	//
	// Insertion sort for a standard ArrayList - update this later please
	public void addRecordSorted(TipEntry entry){
		if(tipLog.size() == 0){
			tipLog.add(entry); 	//if the tip record is empty, just add it
			return;				//and return
		}
		
		int i = getDateAddOffset(entry);
		if(i >= tipLog.size()){
			tipLog.add(entry);
		}
		else {
			tipLog.add(i, entry );
		}
	}
	
	
	public void addTipee(String name){
		tipeeList.add(name);
	}
	
	public void editTipee(int index, String name){
		tipeeList.set(index, name);
	}
	
	public void removeTipee(String name){
		tipeeList.remove(name);
	}
	
	//assign a new tipee list, and wage info from edit info
	public void setTipeeList( ArrayList<String> newList, double hourlyWage, int week){
		tipeeList = newList;
		payRate = hourlyWage;
		weekStart = week;
	}
	
	public double getPayRate() {
		return payRate;
	}
	
	public void removeTipEntry(int index){
		tipLog.remove(index);
	}
	
	public TipEntry getTipEntryByOffset(int x){
		return tipLog.get(x);
	}
	
	//
	// Returns a reference to a tipentry in the log if an entry
	//	for the specified date is present.  Else returns null;
	public TipEntry getTipEntryByDate(int year, int month, int day){
				
		int offset = 0;
		TipEntry target = new TipEntry(year, month, day);	//temporary tipentry to find the offset
		offset = Collections.binarySearch(tipLog, target, new TipEntryComparator<TipEntry>()); // find where it goes
		
		if (offset < 0) {
			return null;  //returns null if nothing found
		}
		else {
			return getTipEntryByOffset(offset);
		}
	}
	
	//
	// Returns a reference to a tipentry in the log if an entry
	//	for the specified date is present.  Else returns null;
	public int getTipEntryOffsetByDate(int year, int month, int day){		
		int offset = 0;
		TipEntry target = new TipEntry(year, month, day);	//temporary tipentry to find the offset
		offset = Collections.binarySearch(tipLog, target, new TipEntryComparator<TipEntry>()); // find where it goes
		
		if(offset >= 0){
			return offset;
		}
		else{		
			return -1;//since the record was pulled from the tiplog, no need to error check here
		}
	}
	
	public ArrayList<String> getTipeeList() {
		return tipeeList;
	}
	
	public ArrayList<TipEntry> getTipLog() {
		return tipLog;
	}
	
	//
	// Returns an arraylist of TipEntry with the range specified (both inclusive)
	//	Returns null if invalid parameters were specified.
	public ArrayList<TipEntry> getTipInfoInRange(int fromOffset, int toOffset){
		
		//if incorrect from and to dates were chosen
		if( fromOffset > toOffset || fromOffset >= tipLog.size() || toOffset < 0)
		{
			return null;
		}
				
		ArrayList<TipEntry> rangeList = new ArrayList<TipEntry>();	//create new ArrayList
		
		if(toOffset == tipLog.size()){		//if to offset == size, the to date was greater than all
			--toOffset;						//so decrement to last entry in list
		}
		
		for( int i = fromOffset; i <= toOffset; i++){
			rangeList.add(getTipEntryByOffset(i));
		}
		
		return rangeList;
	}
	
	
	//
	// Helper method to find the correct offset to add a tipentry
	private int getDateAddOffset(TipEntry targetDate){
		
		int offset = 0;
		 offset = Collections.binarySearch(tipLog, targetDate, new TipEntryComparator<TipEntry>()); // find where it goes
		 if (offset < 0) {		//if it doesnt exist in the list already
			 offset = ~offset;  //get its complement to return 
		 }
		 else {					//otherwise duplicate found, delete the old one
			 tipLog.remove(offset);
		 }
		 
		 return offset;
	}
	
	//
	// Returns the index of the first element >= the specified date
	public int getRangeFromIndex(int year, int month, int day){
				
		int offset = 0;
		TipEntry target = new TipEntry(year, month, day);	//temporary tipentry to find the offset
		offset = Collections.binarySearch(tipLog, target, new TipEntryComparator<TipEntry>());
		
		if (offset < 0) {
			offset = ~offset;
		}
		
		return offset;
	}
	
	
	//
	// Returns the index of the first element >= the specified date
	public int getRangeToIndex(int year, int month, int day){
		
		TipEntry target = new TipEntry(year, month, day);	//temporary tipentry to find the offset
		int offset = 0;
		offset = Collections.binarySearch(tipLog, target, new TipEntryComparator<TipEntry>());
		
		if(offset < 0) {		//if specific date wasnt found
			offset = ~offset;			//if specific date not found, convert to next highest
			
			if(offset < tipLog.size()){
				if (offset == 0){										//if first index was returned
					if ( tipLog.get(offset).compareTo(target) > 0   ) {		//if the first record in the log is greater than target
						return -1;											//return invalid
					}
					else {
						return offset;										//else return the 0
					}
				}
			}
			
			return (offset-1);	//if offset was < 0, binarySearch returned the next highest offset,
								//  which isn't in our desired range
		}
		else {					//otherwise it was found
			return offset;
		}
				
	}
	
	//Adds up the gross tips from an arraylist of entries
	public double getGrossTipTotal(ArrayList<TipEntry> theList){
		int size = theList.size();
		double amt = 0;
		
		for (int i = 0; i < size; i++){
			amt += theList.get(i).getGrossTips();
		}
		
		return (Math.round(amt * 100) /100.0);
	}
	
	//
	//Returns a double of the total tipouts, and populates two arraylists (parameters) with corresponding
	//	tipout recipient and total.  Ensures each tipee is listed only once.
	//
	public double getTipoutTotals(ArrayList<TipEntry> theList, ArrayList<String> theNames, ArrayList<Double> theAmounts){
		double tipoutTotal = 0;
		int size = theList.size();
		
		for(int i = 0; i < size; i++){		//for each date saved
			TipEntry currentEntry = theList.get(i);		//get the current tipentry
			ArrayList<TipoutRecord> currentEntryTipouts = currentEntry.getTipoutInfo();//get the tipout record for the entry
			
			
			if(currentEntryTipouts != null) {	//if we didn't get a bad object
				int temp = currentEntryTipouts.size();	//save its size
				for(int j = 0; j < temp; j++){			//for all records today
					TipoutRecord currentRecord = currentEntryTipouts.get(j);//get the record
					String name = currentRecord.getName();					//store the name
					double amt = currentRecord.getAmount();					//store the amount
					
					if(theNames.contains(name)){	// if a record was already saved for the name
						int offset = theNames.indexOf(name);		//get it's current offset
						double y = theAmounts.get(theNames.indexOf(name));			//store its current value
						y += amt;									//add this tip info to it
						theAmounts.set(offset, y);					//replace it in the arraylist						
					}
					else {						//otherwise we'll add it
						theNames.add(name);
						theAmounts.add(amt);	//these should always correspond due to error checking in the ui
					}
					
					tipoutTotal += amt;		//add this tip's info to the net tipout total
				}
				
			}
			
		}
		
		
		return Math.round(tipoutTotal * 100) /100.0;
	}
	
	public double getNetTipsTotal(ArrayList<TipEntry> theList){
		double netTips = 0;
		int size = theList.size();
		
		
		for(int i = 0; i < size; i++){
			TipEntry currentEntry = theList.get(i);		//get the current tipentry			
			netTips += currentEntry.getNetTips();			
		}
		
		
		return (Math.round(netTips*100) /100.0);   //this isn't rounded
	}
	
	public double getWagesTotal(ArrayList<TipEntry> theList){
		double totalWages = 0;
		int size = theList.size();
		
		for(int i=0; i<size; i++) {
			totalWages += theList.get(i).getWagesEarned();
		}
		
		return (Math.round(totalWages * 100) / 100.0);
	}
	
	public double getDPHTotal(ArrayList<TipEntry> theList){  //the total DPH of a period is the average of their
		double totalDPH = 0;								 //individual DPH's
		int size = theList.size();
		
		for(int i = 0; i < size; i++){
			totalDPH += theList.get(i).getDollarsPerHour(); //add each individual dollars per hour value
		}
		totalDPH = totalDPH/size;  //find their average
		
		return (Math.round(totalDPH * 100)/ 100.0);
	}
	
	public void replaceTipLog(ArrayList<TipEntry> newLog){
		tipLog = newLog;
	}
	
	protected void deleteData(){		
		saveBackup();			//backup the file before we remove it
		
		tipLog = new ArrayList<TipEntry>();	//replace contents with blanks
		tipeeList = new ArrayList<String>();
		payRate = 0;
		saveData();							//save it
	}
	
	protected void saveBackup(){
		FileOutputStream fout;
    	ObjectOutputStream oout;

    	try {
    	  fout = openFileOutput(BACKUP_FILENAME, Context.MODE_PRIVATE);	//open the outputstream
    	  oout = new ObjectOutputStream(fout);							//instantiate the objectout stream
    	  
    	  oout.writeObject(this);
    	  
    	  oout.close();
      	  fout.close();
    	} catch (Exception e) {
    	  e.printStackTrace();
    	}
	}
	
	protected void saveData(){
    	FileOutputStream fout;
    	ObjectOutputStream oout;

    	try {
    	  fout = openFileOutput(FILENAME, Context.MODE_PRIVATE);		//open the outputstream
    	  oout = new ObjectOutputStream(fout);							//instantiate the objectout stream
    	  
    	  oout.writeObject(this);
    	  
    	  oout.close();
      	  fout.close();
    	} catch (Exception e) {
    	  e.printStackTrace();
    	}    	
    }
	
    public void loadBackupTipRecord() {
    	File f = new File(this.getFilesDir(), TipRecord.BACKUP_FILENAME);   
    	
    	if(f.exists()){
        	if(f.length() != 0){
        		try {
					 
    				FileInputStream fin = new FileInputStream(f);
    				ObjectInputStream oin = new ObjectInputStream(fin);
    				TipRecord savedTipRecord = (TipRecord) oin.readObject();
    				oin.close();
    				fin.close();
    					
    				if(savedTipRecord != null){ //if tiprecord found
    					replaceTipLog(savedTipRecord.getTipLog()); //replace it with backup
    					setTipeeList(savedTipRecord.getTipeeList(), savedTipRecord.getPayRate(), savedTipRecord.getWeekStart());//
    					saveData();//and save it
    					Toast.makeText(getApplicationContext(), "Backup loaded", Toast.LENGTH_SHORT).show(); //change this later, model shouldn't know about view
    				}
    				
    			} 
    			catch (FileNotFoundException e) {
    				e.printStackTrace();
    			} 
    			catch (IOException e1) {
    				e1.printStackTrace();
    			}			
    			catch (ClassNotFoundException e) {
    				e.printStackTrace();
    			}
        		
        	}
        	else {
				Toast.makeText(getApplicationContext(), "No backup file to load", Toast.LENGTH_SHORT).show(); //same 
			}
    	}
    	else{
    		Toast.makeText(getApplicationContext(), "No backup file to load", Toast.LENGTH_SHORT).show();		//same
    	}
    }
    
   public void setWeekStart(int n) {
	   weekStart = n;
   }
   
   public int getWeekStart(){
	   return weekStart;
   }
   
   public void toggleShowCalendar(){
	   disableCalendar = !disableCalendar;
   }
   
   public boolean isCalendarDisabled(){
	   return disableCalendar;
   }
    
}
     
