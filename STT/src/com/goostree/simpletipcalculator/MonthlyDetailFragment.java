package com.goostree.simpletipcalculator;

import java.util.ArrayList;
import java.util.Calendar;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MonthlyDetailFragment extends Fragment{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View rootView = inflater.inflate(R.layout.fragment_view_monthly, container, false);
		ViewTipActivity viewActivity = (ViewTipActivity)getActivity();
		TipRecord tipRecord = (TipRecord)viewActivity.getApplicationContext();
		boolean valuesAdded = false;
		LinearLayout mainLayout = (LinearLayout)rootView.findViewById(R.id.monthly_layout);
		
		double highest = 0;
		TextView highestDate = new TextView(getActivity());
		
		Calendar firstMonth = viewActivity.dateList.get(0).getDate();//first date in the list
		Calendar lastMonth = viewActivity.dateList.get(viewActivity.dateList.size() -1).getDate();//last date in the list
		
		firstMonth = (Calendar)firstMonth.clone();
		lastMonth = (Calendar)lastMonth.clone();//clone them to leave originals intact (laziness consumes me)
		
		firstMonth.set(Calendar.DATE, 1);
		lastMonth.set(Calendar.DATE, 1);//set each to the first day of the month
		
		Calendar currentMonth = (Calendar)firstMonth.clone();
		
		while(currentMonth.compareTo(lastMonth) <= 0){  //do while we're within the months requested
			int i =0;
			
			Calendar endOfMonth = (Calendar)currentMonth.clone();
			endOfMonth.set(Calendar.DATE, endOfMonth.getActualMaximum(Calendar.DATE)); //set the end of the month
			
			int from = tipRecord.getRangeFromIndex(currentMonth.get(Calendar.YEAR), //get indexes into the tiprecord
					currentMonth.get(Calendar.MONTH), 								//for begin and end entries in the range
					currentMonth.get(Calendar.DATE));
			int to = tipRecord.getRangeToIndex(endOfMonth.get(Calendar.YEAR), 
					endOfMonth.get(Calendar.MONTH), 
					endOfMonth.get(Calendar.DATE));
			
			ArrayList<TipEntry> currentList = tipRecord.getTipInfoInRange(from, to);
			
			if( currentList != null){
				TextView date = new TextView(getActivity());
				date.setTypeface(null, Typeface.BOLD);
				date.setText(getMonthName(currentMonth.get(Calendar.MONTH), currentMonth.get(Calendar.YEAR)));
				
				if( valuesAdded) {
					date.setPadding(0, 30, 0, 0);
				}
				
				double wageTotal = tipRecord.getWagesTotal(currentList);
				double tipsTotal = tipRecord.getNetTipsTotal(currentList);
				
				if( (wageTotal + tipsTotal) > highest ){
					highest = wageTotal + tipsTotal;
					highestDate = date;
				}
				
				TextView wages = new TextView(getActivity());
				wages.setText("Wages: " + 
						wageTotal);
				
				TextView tips = new TextView(getActivity());
				tips.setText("Tips: " + tipsTotal);
				tips.setGravity(Gravity.CENTER);
				
				TextView dph = new TextView(getActivity());
				dph.setText("$/hr: " + 
						tipRecord.getDPHTotal(currentList));
				dph.setGravity(Gravity.RIGHT);
				
				LinearLayout innerLayout = new LinearLayout(getActivity());
				innerLayout.setOrientation(LinearLayout.HORIZONTAL);
				
				
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				
				LinearLayout.LayoutParams innerParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
				
				innerLayout.addView(wages, innerParams);
				innerLayout.addView(tips, innerParams);
				innerLayout.addView(dph, innerParams);
				
				mainLayout.addView(date);
				mainLayout.addView(innerLayout, params);
				
				valuesAdded = true;
			}
			
			currentMonth.add(Calendar.MONTH, 1);
			
		}

		char c = '\u2713';
		highestDate.setText( highestDate.getText().toString() + " " + Character.toString(c));
		
		return rootView;
	}
	
	private String getMonthName(int month, int year){
		String name;
		
		switch(month){
		case 0:
			name = "January";
			break;
		case 1:
			name = "February";
			break;
		case 2:
			name="March";
			break;
		case 3:
			name = "April";
			break;
		case 4:
			name = "May";
			break;
		case 5:
			name = "June";
			break;
		case 6:
			name = "July";
			break;
		case 7:
			name = "August";
			break;
		case 8:
			name = "September";
			break;
		case 9:
			name = "October";
			break;
		case 10:
			name = "November";
			break;
		case 11:
			name = "December";
			break;
		default:
				name = "";
		}
		
		name = name + " " + year;
		return name;
	}
}
